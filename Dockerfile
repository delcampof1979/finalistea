#Descarga la imagen nginx del repo de Docker
FROM nginx
#Crea la varible de entorno que se usara para dar el nombre a la aplicacion
ENV APP_TITLE "CALCULADORA CH"
#Crea la variable de entorno que dara nombre al sitio
ENV Website_Vhost _
#Uilizamos WORKDIR para posicionarnos en ese directorio
WORKDIR /var/www/html
#Utilizamos RUN para ejecutar un comando bash dentro del contenedor, en este caso crear una carpeta
RUN mkdir img
#Ahora copiamos la configuracion para nginx
COPY default /etc/nginx/conf.d/example.conf
#Aqui copiamos nuestra aplicacion html al directorio de nginx donde la publicaremos
COPY index.html /var/www/html
#Corremos el comando sed para reemplazar el nombre de la aplicacion por el de la variable de entorno definida anteriormente
RUN sed -i 's/APP_TITLE/'"${APP_TITLE}"'/' /var/www/html/index.html
#Corremos el comando sed para reemplazar el nombre del sitio por el de la variable de entorno definida anteriormente
RUN sed -i 's/server_name _;/server_name '"${Website_Vhost}"';/' /etc/nginx/conf.d/example.conf
#Aqui exponemos el contenedor en el puerto 80
EXPOSE 80
