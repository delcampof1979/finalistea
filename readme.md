# TRABAJO PRACTICO FINAL - ACTUALIZACION TECNOLOGICA


## TAREA: DESPLIEGUE DE APP CALCULADORA SOBRE NGINX CONTENERIZADA CON DOCKER

## Informacion

- Alumno: Federico del Campo
- Materia: Actualizacion Tecnologica
- Repositorio Gitlab: https://gitlab.com/delcampof1979/finalistea
- private id_rsa: Adjunta en entrega


# Tabla de Contenidos
- 1. Descripcion de actividades
- 2. Requisitos
- 3. Instalacion de Docker
- 4. Instalacion Gitlab Client
- 5. Clonacion de archivos para crear contenedor
    - 	5.1 Instalacion de llave (adjunta en entrega de TP)
    - 	5.2. Sincronizacion local con el repositorio remoto
- 6. Explicacion de cada archivo y sus instrucciones
    - 	6.1. finalistea/default
    - 	6.2. finalistea/index.html
    - 	6.3. finalistea/Dockerfile
    - 	6.4. finalistea/readme.md
    - 	6.5. finalistea/img/imagen.png 
    - 	6.6. finalistea/img/docker.png
- 7. Despliegue del contenedor con la aplicacion
    - 	7.1. Creacion de la imagen
    - 	7.2. Despliegue del contenedor con la aplicación
- 8. Verificacion del funcionamiento de la Aplicacion
- 9. Fuentes


## Descripcion de Actividades

En el presente TP desplegaremos una calculadora realizada integramente en HTML. La misma correra sobre un servidor nginx que hara de servidor web que estara dentro de un contenedor realizado con Docker. La aplicacion consumira recursos que estan fuera del contenedor mediante la creación de un volumen, y el nombre que mostrara la aplicacion al ejecutarse podra modificarse mediante una variable de entorno.
Tanto los archivos necesarios para el despliegue de esta aplicación como sus instrucciones se encuentran en el repositorio informado 

## Requisitos:

 - PC con sistema operativo Linux
 - Llave privada id_rsa provista en entrega

# Instalacion de Docker

Actualizamos el SO e instalamos los paquetes necesarios para comunicarnos por HTTPS

    $ sudo apt-get update
    
    $ sudo apt-get install \
        ca-certificates \
        curl \
        gnupg \
        lsb-release
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_1.png)
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_2.png)
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_3.png)
Agregamos la Gnu Privacy Guard Oficial:

```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_4.png)
Agregamos los repositorios oficiales "estables"
```
$ echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
Instalamos Docker Engine
```
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_5.png)
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_6.png)

# Instalacion Gitlab Client
Instalacion y configuracion de dependencias necesarias

    $ sudo apt-get update
    $ sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_9.png)

Instalacion de Postfix para envio de notificaciones por email

    $ sudo apt-get install -y postfix
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_10.png)

Agregamos el repositorio de paquetes de Gitlab e instalamos el cliente

    $ curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

# Clonacion de archivos para crear contenedor

## Instalacion de llave (adjunta en entrega de TP)
Verificamos si existe la carpeta /root/.ssh, y si no existe la creamos

    $ mkdir /root/.ssh

Ingresamos a la carpeta creada (o ya existente)

    $ cd /root/.ssh

Copiamos la llave id_rsa provista y verificamos

    $ root@ubuntu:~/.ssh# ls
    id_rsa
Procedemos a establecer los permisos apropiados para utlizar la misma

    chmod 400 id_rsa
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_11.png)
## Sincronizacion local con el repositorio remoto
Ahora procedemos a ir a la ubicacion donde querramos que se cree la carpeta sincronizada con el repositorio remoto en Gitlab

    $ cd /home/fcampo/git
Ahora procedemos a sincronizar la carpeta con el repo en Gitlab

    $ git clone git@gitlab.com:delcampof1979/finalistea.git
Deberiamos tener una nueva carpeta llamada "finalistea" con todos los archivos necesarios para crear la APP contenerizada
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_13.png)

# - Explicacion de cada archivo y sus instrucciones
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_19.png)

    root@ubuntu:/home/fcampo/git# tree finalistea
    finalistea
    ├── default
    ├── Dockerfile
    ├── readme.md
    ├── img
    │   ├── docker.png
    │   └── imagen.png
    └── index.html

## finalistea/default
Contiene la configuracion del servidor nginx

       server {
                listen 80 default_server; # Puerto de escucha del servidor
                root /var/www/html; #Directorio root de la aplicacion
        		index index.html; #Archivo index de la aplicacion
                server_name _; #Nombre por default del servidor
        }

## finalistea/index.html
Contiene la aplicacion calculadora que vamos a ejecutar

## finalistea/Dockerfile
Archivo que contiene la informacion necesaria para la creacion del contenedor

    #Descarga la imagen nginx del repo de Docker
    FROM nginx
    #Crea la varible de entorno que se usara para dar el nombre a la aplicacion
    ENV APP_TITLE "CALCULADORA ISTEA"
    #Crea la variable de entorno que dara nombre al sitio
    ENV Website_Vhost _
    #Uilizamos WORKDIR para posicionarnos en ese directorio
    WORKDIR /var/www/html
    #Utilizamos RUN para ejecutar un comando bash dentro del contenedor, en este caso crear una carpeta
    RUN mkdir img
    #Copia la configuracion para nginx y cambia su nombre a example.conf
    COPY default /etc/nginx/conf.d/example.conf
    #Aqui copiamos nuestra aplicacion html al directorio de nginx donde la publicaremos
    COPY index.html /var/www/html
    #Corremos el comando sed para reemplazar el nombre de la aplicacion por el de la variable de entorno definida anteriormente
    RUN sed -i 's/APP_TITLE/'"${APP_TITLE}"'/' /var/www/html/index.html
    #Corremos el comando sed para reemplazar el nombre del sitio por el de la variable de entorno definida anteriormente
    RUN sed -i 's/server_name _;/server_name '"${Website_Vhost}"';/' /etc/nginx/conf.d/example.conf
    #Aqui exponemos el contenedor en el puerto 80
    EXPOSE 80

## finalistea/readme.md
Este es el archivo readme.md que estas leyendo en este momento.
## finalistea/img/imagen.png
Es la imagen que se utilizara como logo de la aplicación. Esta imagen se vinculara al contenedor mediante la creacion de un volumen. Si deseamos cambiar el logo, simplemente tenemos que cambiar la imagen sin necesidad de re armar / desplegar el contenedor.

## finalistea/img/docker.png
Es una imagen alternativa. Se incluyo con el unico proposito de que puedan testear que se puede reemplazar la imagen sin necesidad de re armar / desplegar el contenedor

# Despliegue del contenedor con la aplicacion
## Creacion de la imagen
Una vez realizados todos los pasos anteriores, procederemos a crear el contenedor

    docker build -t nginx-calculadora .

 - **build:** el parametro de docker para construir contenedor
 - **-t:** asigna el tag "nginx-calculadora" al contenedor
 - **"."** Indica que el archivo Dockerfile se encuentra el el directorio actual

![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_15.png)

Ahora validaremos la creacion de la imagen y deberiamos verla en la lista presentada

    $ docker images
    
    REPOSITORY          TAG       IMAGE ID       CREATED         SIZE
    nginx-calculadora   latest    a285e1f8cfbf   5 minutes ago   141MB

## Despliegue del contenedor con la aplicación

Procedemos a desplegar el contenedor con el siguiente comando:

    docker run -d -p 80:80 --name calculadora -v /home/fcampo/git/finalistea/img/:/var/www/html/img/ nginx-calculadora:latest

 - **run:** el parametro de docker para desplegar una imagen ya creada
 - **-d:** despliega el contenedor en modo "daemon" (nos permitira seguir interactuando con la consola de nuestro Sistema Operativo)"
 - **-p:** mapea el puerto de escucha del contenedor con el de la aplicacion. Si quisieramos que el contenedor escuche en otro puerto, por ejemplo el 8090, simplemente ponemos 8090:80. De esta forma nos ahorramos la necesidad de reconfigurar el nginx.
 - **-v:** con este paremetro crearemos el volumen por el cual, el directorio local **"/home/fcampo/git/finalistea/img/"** quedara vinculado con el directorio del contenedor **"/var/www/html/img/"**. Todo lo que se guarde en el primero sera visible en el segundo, pero si el contenedor se elimina, los archivos persistiran en el directorio original, ya que los mismos se encuentran FUERA del contenedor.
  - **--name:** especifica el nombre del contenedor
 - **nginx-aep:latest:** aqui es donde le decimos el nombre de la imagen que queremos desplegar. el "latest" significa que queremos la ultima version de la misma (en el caso de que exista mas de una)
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_18.png)
Para validar el correcto despliegue ejecutamos el siguiente comando y verificamos que el contenedor se encuentre presente en la lista presentada

    $ docker images

     CONTAINER ID   IMAGE                      COMMAND                  CREATED              STATUS              PORTS                               NAMES
    cb509191abeb   nginx-calculadora:latest   "/docker-entrypoint.…"   About a minute ago   Up About a minute   0.0.0.0:80->80/tcp, :::80->80/tcp   calculadora
![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_17.png)
# Verificacion del funcionamiento de la Aplicacion
Como primer paso averiguamos el ip de la pc donde desplegamos el contenedor:

    $ ip a
    2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 00:0c:29:47:1f:fc brd ff:ff:ff:ff:ff:ff
        altname enp2s1
        inet 192.168.10.23/24 brd 192.168.10.255 scope global dynamic noprefixroute ens33
           valid_lft 442sec preferred_lft 442sec
        inet6 fe80::6efa:d7b3:cee8:16dd/64 scope link noprefixroute
           valid_lft forever preferred_lft forever
Una vez obtenida la ip (en nuestro caso 192.168.10.23), procedemos a ingresar a la misma desde algun navegador. En nuestro caso accedimos desde otra pc con Windows 10 dentro de la misma red, aunque tranquilamente puede probarse desde la misma pc donde se desplego el contenedor, si la misma dispone de interfaz grafica y un navegador, claro esta.

![enter image description here](https://finalistea.s3.amazonaws.com/Screenshot_16.png)

Aqui podemos verificar que tanto la imagen con el logo ubicada en home/fcampo/git/finalistea/img/ como el nombre "CALCULADORA ISTEA" enviado mediante la variable de entorno APP_TITLE en el  Dockerfile se muestran correctamente.

# Fuentes

 - https://www.geeksforgeeks.org/html-calculator/
 - https://about.gitlab.com/install/#ubuntu
 - https://docs.docker.com/engine/install/ubuntu/
